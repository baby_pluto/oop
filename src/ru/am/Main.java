package ru.am;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        Category clothes = new Category("Одежда", List.of(
                new Product("Куртка", 15000, 7),
                new Product("Штаны", 5000, 8),
                new Product("Носки", 100, 10),
                new Product("Кроссовки", 20000, 8)));

        Category cars = new Category("Автомобили", List.of(
                new Product("BMW", 75000, 8),
                new Product("Mercedes", 75000, 8),
                new Product("Volkswagen", 35000, 6),
                new Product("Porsche", 250000, 10)));

        User user = new User("MyLogin", "MyPassword", new Basket(List.of(
                clothes.getProductByName("Носки"),
                clothes.getProductByName("Кроссовки"),
                cars.getProductByName("BMW"))));
    }
}